﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace PresentationV12
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Example 1 LINQ Query
            using (var repository = new Database1Entities())
            {
                var query = repository.Users.Where(user => user.UserId == 1);
            }
            #endregion

            #region Example 2 LINQ With Result

            using (var repository = new Database1Entities())
            {
                var usr = repository.Users.Where(user => user.Username.EndsWith("Brian")).FirstOrDefault();
            }
            #endregion

            #region Example 3 Expression

            using (var repository = new Database1Entities())
            {
                Expression<Func<User, bool>> expression = user => user.UserId != 5;

                //Entity framework does similar parsing to the below and translates the expression to SQL
                ParameterExpression param = (ParameterExpression)expression.Parameters[0];
                BinaryExpression operation = (BinaryExpression)expression.Body;
                var left = operation.Left;
                var right = operation.Right;

                var usr = repository.Users.Where(expression).FirstOrDefault();
            }
            #endregion

            #region Example 4 Not using Expressions - LINQ To objects
            using (var repository = new Database1Entities())
            {
                //Extension for IQueryable
                var usrs = repository.Users.Where(user => user.UserId != 5).ToList();

                //Different extension method, IEnumerable.  
                var user2ndQuery = usrs.Where(user => user.UserId != 6);
            }
            #endregion

            #region Example 5 Compiling Expression Tree

            using (var repository = new Database1Entities())
            {
                Expression<Func<User, bool>> expression = user => user.UserId != 5;

                var usrs = repository.Users.ToList();

                //Can't query objects with Expression<Func<User, bool>> expression
                //Error - var user2ndQuery = usrs.Where(expression);

                //But you can reuse that expression like this to do that v
                var compiledExpression = expression.Compile();
                var user2ndQuery = usrs.Where(compiledExpression);
            }
            #endregion

            #region Example 6 Building a dynamic Expression tree 
  
            var param1 = Expression.Parameter(typeof(User), "user"); 
            var prop = Expression.Property(param1, "UserId");
            var Right = Expression.Constant(5);
            BinaryExpression expr = Expression.MakeBinary(ExpressionType.NotEqual, prop, Right);
 
            var userExpression = Expression.Lambda<Func<User, bool>>(
                expr,
                param1
                ).Compile();

            using (var repository = new Database1Entities())
            {
                var usr = repository.Users.Where(userExpression).ToList();
            }

            #endregion

            #region My Number 1 Tip for writing good LINQ
            //Compose your queries so you don't repeat yourself
            //Lack of easy composability in SQL is the worst crux of writing SQL
            //If you're copying and pasting a lot, you're doing it wrong
            using (var repository = new Database1Entities())
            {
                var userQuery = repository.Users
                    .Where(UserNotTwo())
                    .Where(UserNotBrian()).Where(NewMethod());

                var usrList = userQuery.ToList(); //lazy evaluation "combines" both criteria 
            }
            #endregion

            #region SelectMany Flattening
            //Flattens collections of collections
            //Fairly common for a collection to contain collections with navigation properties in Entity Framework
            //Necessary because there's no looping in a single LINQ expression--You'd have to bring the data down and loop yourself
            //http://blog.falafel.com/Blogs/adam-anderson/2010/06/29/Flatten_Nested_Loops_with_LINQ_and_SelectMany

            using (var repository = new Database1Entities())
            {
                var complexList = new List<List<string>>() { new List<string> { "test", "test2" }, new List<string> { "test3", "test4" } };
                var easyList = complexList.SelectMany(lst => lst);
                
                var superComplexList = new List<List<object>>() { new List<object> { "test", "test2", 25 }, new List<object> { "test3", "test4", new List<string>{ "test"} } };
                var kindOfEasyList = superComplexList.SelectMany(lst => lst);
            }
            #endregion
        }

        private static Expression<Func<User, bool>> NewMethod()
        {
            return tt => tt.Email != "brosamilia@example.com";
        }

        #region Helper Functions
        private static Expression<Func<User, bool>> UserNotBrian()
        {
            return usr => usr.FirstName != "Brian";
        }

        private static Expression<Func<User, bool>> UserNotTwo()
        {
            return user => user.UserId != 2;
        }
        #endregion
    }
}
